import datetime

from django.shortcuts import render
from django.views import View
from recorde_core.models import Record, Client, ClientData
from doctors.models import Doctor
from registerapp.models import RegisterWorker
from django.views.generic import ListView, DetailView, CreateView



# Create your views here.
class RegisterPage(View):
    def get(self, request):
        return render(request=request, template_name='registerBase.html')


class CreateRecord(CreateView):
    model = Record
    template_name = 'RegisterCreateRecord.html'
    fields = '__all__'
    success_url = 'registerpage//registerTodayRecord/'



class TodayRecordList(ListView):
    queryset = Record.objects.filter(date_record=datetime.date.today())
    context_object_name = 'record'
    template_name = 'RegisterTodayRecord.html'



class DoctorList(ListView):
    queryset = Doctor.objects.all()
    context_object_name = 'doctor'
    template_name = 'RegisterDoctorList.html'



class ClientList(ListView):
    queryset = Client.objects.all()
    context_object_name = 'client'
    template_name = 'RegisterClientList.html'

class CreateCleint(CreateView):
    model = Client
    fields = '__all__'
    template_name = 'RegisterCreatCleint.html'
    success_url = 'regClientList'


