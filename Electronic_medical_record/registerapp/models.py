from django.db import models

# Create your models here.
class RegisterWorker(models.Model):
    name = models.CharField('ФИО сотрудника',max_length=255 )
    number = models.CharField('номер телефона', max_length=20,
                              blank=True)
    email = models.EmailField('рабочая почта', max_length= 255,
                              default=False)
    login = models.CharField('Логин', max_length=255)
    create_date = models.DateField('дата регистрации',
                                   auto_now_add=True)


    def __str__(self):
        return self.name