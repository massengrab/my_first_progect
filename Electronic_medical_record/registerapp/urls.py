from django.urls import path
from registerapp.views import RegisterPage, CreateRecord, DoctorList, ClientList, CreateCleint, TodayRecordList

urlpatterns =[
    path('', RegisterPage.as_view(), name='registerpage'),
    path('createRecord/', CreateRecord.as_view(),
         name='registerCreate'),
    path('registerDoctrorsList/', DoctorList.as_view(),
          name='registerDocList'),
    path('registerClientList/', ClientList.as_view(),
         name='regClientList'),
    path('registerClientCreate/', CreateCleint.as_view(),
         name='regClientCreate'),
    path('registerTodayRecord/', TodayRecordList.as_view(),
         name='regTodayRecord')

]