from django.apps import AppConfig


class MaindoctappConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'maindoctapp'
