from django.shortcuts import render
from django.views import View
from django.views.generic import ListView, DetailView
from doctors.models import Doctor
from doctors import models



class Doctor_page(View):
    def get(self,request):
        doctor = models.Doctor.objects.all()
        context = {'doctor':doctor}
        return render(request=request, template_name='doctor_page.html', context=context)



