from django.contrib.auth.models import User
from django.db import models

class Doctor(models.Model):
    name = models.OneToOneField(User, related_name='user_name', on_delete=models.CASCADE)

    special = models.CharField(max_length=50,
                                    blank= True)
    second_special = models.CharField(max_length=50,
                                    blank=True)
    is_sector_doc = models.BooleanField(default=False)


    def __str__(self):
        return self.get_full_name()

    def get_full_name(self) ->str:

            return f'{self.name}'


    def get_special(self):
        special = self.special
        return special

    def get_second_special(self):
        second_special = self.second_special
        return second_special

class Sector(models.Model):
    sector_number = models.IntegerField()
    sector_name = models.CharField(max_length=20,blank=True)
    def __str__(self):
        return self.sector_name
class Main_doctor(models.Model):
    name = models.OneToOneField(Doctor, related_name='doc_name',
                                on_delete=models.PROTECT)
    sector = models.OneToOneField(Sector,related_name='sector',
                                  on_delete=models.PROTECT)
    def __str__(self):
        return str(self.name)

