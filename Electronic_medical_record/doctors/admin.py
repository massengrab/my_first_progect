from django.contrib import admin
from doctors.models import Doctor, Sector, Main_doctor
from recorde_core.models import Client, Record
# Register your models here.
admin.site.register(Doctor)
admin.site.register(Sector)
admin.site.register(Main_doctor)
