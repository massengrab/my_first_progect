from django.urls import path
from recorde_core.views import Homepage, Main_info, ClientArea

urlpatterns = [
    path('', Homepage.as_view()),
    path('main_info/', Main_info.as_view(), name='main_info'),
    path('clientArea/', ClientArea.as_view(), name='ClientArea')
]