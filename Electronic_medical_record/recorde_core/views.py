import datetime

from django.shortcuts import render
from django.urls import reverse_lazy
from django.views import View
from django.views.generic import DetailView, ListView, CreateView, UpdateView
from recorde_core.models import Client, Record
from recorde_core import models
from recorde_core.forms import Create_record, Record_text_create
from recorde_core import forms


# Create your views here.
class Homepage(View):
    def get(self,request):
        return render(request=request, template_name='core/mainpage.html')
class Main_info(View):
    def get(self,request):
        return render(request=request, template_name='core/main_info.html')



class ClientArea(DetailView):
    models = Client
    template_name = 'core/clientArea.html'
    context_object_name = 'client'
    slug_field = Client.phoneNumber
