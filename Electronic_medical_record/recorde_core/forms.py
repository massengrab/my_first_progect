from  django import forms

class Create_record(forms.Form):
    client_name = forms.CharField(max_length=30, label='Имя пациента')
    doctor_name = forms.CharField(max_length=30, label='Имя врача')
    date_record = forms.DateField()

class Record_text_create(forms.Form):
    client = forms.CharField(max_length=30, label = 'Имя пациента')
    doctor = forms.CharField(max_length=30, label='Имя врача')
    text = forms.TimeField()
