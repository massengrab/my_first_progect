from django.apps import AppConfig


class RecordeCoreConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'recorde_core'
