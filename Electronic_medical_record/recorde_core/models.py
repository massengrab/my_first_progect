import datetime

from django.contrib.auth.models import User
from django.db import models
from django.utils import timezone
from doctors.models import Doctor, Sector


class Client(models.Model):
    client_name = models.OneToOneField(User, related_name= 'client_name',
                                on_delete=models.CASCADE)
    middle_name = models.CharField(max_length=50, blank=True)
    sex = models.CharField(max_length=15, blank=True)
    sector = models.ForeignKey(Sector, related_name='sec_name',
                               on_delete=models.CASCADE)
    phoneNumber = models.CharField(max_length=15,blank=True)

    def __str__(self):
        return self.get_full_name()

    def get_full_name(self) -> str:
            return f'{self.client_name}'


    def get_sector(self):
        return f'{self.sector}'




class ClientData(models.Model):
    name_data = models.OneToOneField(Client, related_name='clientName',
                                verbose_name='имя клиента',
                                on_delete=models.CASCADE, blank=True)
    bloodType = models.CharField('группа крови',
                                 max_length=50,
                                 blank=True)
    rhFactor = models.CharField('резус фактор',
                                max_length=25,
                                blank=True)
    chronicDiseases = models.TextField(blank=True)
    specialNotes = models.TextField(blank=True)
    bornDate = models.CharField('дата рождения',
                                blank=True,
                                max_length=30)




class Record(models.Model):
    doctor_name = models.ForeignKey(Doctor, related_name='record_doc',
                                       on_delete=models.CASCADE,
                                       verbose_name='Врач')
    client_name = models.ForeignKey(Client, related_name='pacient',
                                       on_delete=models.CASCADE,
                                       verbose_name='пациент')
    date_record = models.DateTimeField(verbose_name='дата записи',default=False, null=True)
    text = models.TextField(blank=True)

    def __str__(self):
        return f'Запись к врачу {self.doctor_name} {self.date_record}'

    def get_name(self):
        return f'{self.client_name}'

    def get_doc(self):
        return f'{self.doctor_name}'

    def get_date(self):
        return f'{self.date_record}'

