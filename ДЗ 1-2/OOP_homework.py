""" Домашняя работа № 2"""



class Counter:

    def __init__(self, initial_value):
        self.initial_value = initial_value

    def inc(self):
        self.initial_value += 1
        return self.initial_value

    def dec(self):
        self.initial_value -= 1
        return self.initial_value


class ReverseCounter(Counter):

    def inc(self):
        self.initial_value -= 1
        return self.initial_value

    def dec(self):
        self.initial_value += 1
        return self.initial_value


def get_counter(number):
    if number >= 0:
        x = Counter(number)
    elif number < 0:
        x = ReverseCounter(number)
    return x


#counter = get_counter()
#a = counter.dec()
#b = counter.dec()
#c = counter.inc()
#d = counter.dec()

#print(a, b, c, d)
