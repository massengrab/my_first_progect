def month_return(d, m, y):
    """ Задача 1"""

    month31 = (1, 3, 5, 7, 8, 10, 12)
    month = {1: 'января', 2: 'февраля', 3: 'марта',
             4: 'апреля', 5: 'мая', 6: 'июня',
             7: 'июля', 8: 'августа', 9: 'сентября',
             10: 'октября', 11: 'ноября', 12: 'декабря'}
    error_date = ('Некоректно введена дата')

    if d > 31 or m > 12:
        return f'{error_date}'
    elif d == 31 and m not in month31:
        return f'{error_date}'
    elif m == 2:
        if d > 29:
            return f'{error_date}'

        if d == 29 and not y % 4 != 0 and (y % 400 == 0 or y % 100 != 0):
            return f'{error_date}'
    return f'{d} {month[m]} {y} года'

def name_dic(*args):
    """Задача 2"""
    name_list = args
    name_dict = {}
    for element in name_list:
        if element not in name_dict:
            name_dict[element] = 1
        if element in name_dict:
            name_dict[element] += 1
    return name_dict


def name_print(dict):

    """Задача 3"""

    first_name = dict.get('first_name')
    last_name = dict.get('last_name')
    middle_name = dict.get('middle_name')
    _name = str()
    if first_name:
        _name += first_name + " "
        if first_name and not last_name:
            return _name
        if last_name:
            _name += last_name
            if middle_name:
                _name += " " + middle_name
                return _name
            else:
                return _name

    if last_name:
        _name += last_name
        return _name
    if middle_name:
        return
    else:

        return 'Нет данных'


def simple_number(n):
    """ Задача № 4 """
    m = 1
    res = False
    while n > m:
        m += 1
        if m == n:
            res = True
            return res
        if n % m != 0:
            continue
        if n % m == 0:
            if n == m:
                res = True
            else:
                break

    return res

#print(simple_number())


def sort_fun(*args):
    """ Задача № 5 """
    arg_list = args
    arg_set = set(args)
    res_list =[]
    for i in arg_set:
        if isinstance(i,int):
            res_list.append(i)
    return res_list

#print(sort_fun(1, '2', 'text', 42, None, None, None, 15, True, 1,))
